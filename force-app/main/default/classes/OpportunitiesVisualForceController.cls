public with sharing class OpportunitiesVisualForceController {
    public static final String URL_EXCEL = '/apex/EXCEL';

    public OpportunitiesVisualForceController() {
        this.opportunities = getClosedWonOpportunitiesInCurrentMonth();
    }

    /*
    @Description gets all closed won opportunities of the current month
    */

    public static List<Opportunity> getClosedWonOpportunitiesInCurrentMonth() {

        Date startCurrentMonth = Date.today().toStartOfMonth();
        Date startNextMonth = startCurrentMonth.addMonths(1);

        return [SELECT 
                    Name, 
                    Account.Name,
                    AccountId, 
                    Amount, 
                    CloseDate, 
                    StageName, 
                    Type
                FROM
                    Opportunity
                WHERE 
                    StageName IN (:AccountTriggerHandler.OPP_STAGE_CLOSED_WON, :AccountTriggerHandler.OPP_STAGE_CLOSED_LOST)
                AND 
                    CloseDate >= :startCurrentMonth
                AND 
                    CloseDate < :startNextMonth
                WITH SECURITY_ENFORCED
                ORDER BY StageName
                LIMIT 1000
            ];
    }
    /*
    @Description returns a page reference to the EXCEL visualforce page
    */

    public static PageReference redirectToExcel() {
        PageReference page = new PageReference(URL_EXCEL);
        page.setRedirect(true);
        return page;
    }

    public List<Opportunity> opportunities { get; set; }
}
