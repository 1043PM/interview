public with sharing class OpportunityQueueable implements Queueable, Database.AllowsCallouts {
    List<Opportunity> closedWonOpportunities;
    public static final String SENT_STATUS = 'Enviado';
    public static final String RESENT_STATUS = 'Reenviar';
    public static final Integer SUCCESS_STATUS_CODE = 200;

    public OpportunityQueueable(List<Opportunity> closedWonOpportunities) {
        this.closedWonOpportunities = closedWonOpportunities;
    }

    public void execute(QueueableContext context) {
        sendClosedWonOppCallout(this.closedWonOpportunities);
    }

    public static void sendClosedWonOppCallout(List<Opportunity> closedWonOpportunities) {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:apexDevnet/test/prueba/');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');

        request.setBody(JSON.serialize(closedWonOpportunities));

        HttpResponse response = http.send(request);

        updateOpportunityByStatusCode(response.getStatusCode(), closedWonOpportunities);
    }

    public static void updateOpportunityByStatusCode(Integer statusCode, list<Opportunity> opportunities) {
        List<Opportunity> oppToUpdate = [SELECT Id, StageName FROM opportunity WHERE Id IN: opportunities WITH SECURITY_ENFORCED LIMIT 200];

        for(Opportunity opp : oppToUpdate) {
            if(statusCode == SUCCESS_STATUS_CODE) {
                opp.StageName = SENT_STATUS;
            } else {
                opp.StageName = RESENT_STATUS;
            }
        }

        update oppToUpdate;
    }
}
