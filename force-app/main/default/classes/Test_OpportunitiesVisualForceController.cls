@isTest
public with sharing class Test_OpportunitiesVisualForceController {
    @TestSetup
    static void makeData(){
        System.runAs(Test_AccountTrigger.createUser('System Administrator')) {
            Account acc = new Account(Name = 'Test account', Activa__c = AccountTriggerHandler.ACC_IS_ACTIVE);
            insert acc;

            List<Opportunity> opportunities = new List<Opportunity>();

            opportunities.add(new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp',
                                                StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_LOST, 
                                                CloseDate = Date.Today()));
            
            insert opportunities;
        }
    }

    @isTest static void getClosedOpportunities_NEGATIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];
        Opportunity opp = [SELECT Id, CloseDate FROM opportunity LIMIT 1];

        opp.CloseDate = Date.today().addMonths(1);

        update opp;

        System.runAs(us) {
            System.Test.startTest();
            OpportunitiesVisualForceController controller = new OpportunitiesVisualForceController();
            System.Test.stopTest();

            System.assertEquals(0, controller.opportunities.size(), 'Should be 0 opportunities');
        }
    }

    @isTest static void getClosedOpportunities_POSITIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            System.Test.startTest();
            OpportunitiesVisualForceController controller = new OpportunitiesVisualForceController();
            System.Test.stopTest();
    
            System.assertEquals(1, controller.opportunities.size(), 'Should be only one opportunity');
        }
    }

    @isTest static void redirect_POSITIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            System.Test.startTest();

            System.assertNotEquals(null, OpportunitiesVisualForceController.redirectToExcel(), 'Should be one object');

            System.Test.stopTest();
        }
    }
}
