@isTest
global with sharing class MockResponse implements HttpCalloutMock {
    public Integer resCode;

    public MockResponse(Integer resCode) {
        this.resCode = resCode;
    }

    global HTTPResponse respond(HTTPRequest req) { 
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(resCode);
        return res;
    }
}
