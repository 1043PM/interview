public with sharing class OpportunityTriggerHandler extends TriggerHandler {
    public override void afterUpdate() {
        List<Opportunity> closedWonOpportunities = getClosedWonOpportunities(); 
        
        if(!closedWonOpportunities.isEmpty()) {
            System.enqueueJob(new OpportunityQueueable(closedWonOpportunities));
        }
    }

    private static List<Opportunity> getClosedWonOpportunities() {
        List<Opportunity> closedWonOpportunities = new List<Opportunity>();
        Map<Id, Opportunity> oldValuesOppMap = (Map<Id, Opportunity>) Trigger.OldMap;

        for(Opportunity opp : (List<Opportunity>) Trigger.new) {
            if(opp.StageName == AccountTriggerHandler.OPP_STAGE_CLOSED_WON && opp.StageName != oldValuesOppMap.get(opp.Id)?.StageName) {
                closedWonOpportunities.add(opp);
            }
        }

        return closedWonOpportunities;
    }
}
