public with sharing class OpportunitiesTableController {
    public class Payload {
        @AuraEnabled public List<Opportunity> opportunities;
    }
    /*
    @Description get closed opportinities of current month by account Id
    */
    @AuraEnabled
    public static Payload getClosedOpportunitiesByAccountId(String accountId) {

        if(String.isEmpty(accountId)) {
            return null;
        }

        Date startCurrentMonth = Date.today().toStartOfMonth();
        Date startNextMonth = startCurrentMonth.addMonths(1);
        Payload payload = new Payload();

        payload.opportunities =  [
            SELECT  
                Name, 
                Account.Name,
                AccountId, 
                Amount, 
                CloseDate, 
                StageName, 
                Type
            FROM 
                Opportunity
            WHERE 
                AccountId = :accountId
            AND 
                StageName IN (:AccountTriggerHandler.OPP_STAGE_CLOSED_WON, :AccountTriggerHandler.OPP_STAGE_CLOSED_LOST)
            AND 
                CloseDate >= :startCurrentMonth
            AND 
                CloseDate < :startNextMonth
            WITH SECURITY_ENFORCED
            ORDER BY StageName
            LIMIT 1000
        ];

        return payload;
    }
}
