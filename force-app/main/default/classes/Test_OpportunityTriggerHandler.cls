@isTest
public with sharing class Test_OpportunityTriggerHandler {
    @TestSetup
    static void makeData(){
        System.runAs(Test_AccountTrigger.createUser('System Administrator')) {
            Account acc = new Account(Name = 'Test account', Activa__c = AccountTriggerHandler.ACC_IS_ACTIVE);
            insert acc;

            List<Opportunity> opportunities = new List<Opportunity>();

            opportunities.add(new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp',
                                                StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_LOST, 
                                                CloseDate = Date.Today()));
            
            insert opportunities;
        }
    }

    @isTest static void UPDATE_OPP_CLOSED_WON_SUCCESS() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            Account acc = [SELECT Id FROM Account WHERE Name =: 'Test account'];
            Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE AccountId =: acc.Id];

            opp.StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_WON;

            Test.setMock(HttpCalloutMock.class, new MockResponse(200));

            System.Test.startTest();
            update opp;
            System.Test.stopTest();

            Opportunity oppAfter = [SELECT Id, StageName FROM Opportunity WHERE AccountId =: acc.Id];
            System.assertEquals(OpportunityQueueable.SENT_STATUS, oppAfter.StageName, 'Should be \'Enviado\'');
        }
    }

    @isTest static void UPDATE_OPP_CLOSED_WON_FAILURE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            Account acc = [SELECT Id FROM Account WHERE Name =: 'Test account'];
            Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE AccountId =: acc.Id];

            opp.StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_WON;

            Test.setMock(HttpCalloutMock.class, new MockResponse(500));

            System.Test.startTest();
            update opp;
            System.Test.stopTest();

            Opportunity oppAfter = [SELECT Id, StageName FROM Opportunity WHERE AccountId =: acc.Id];
            System.assertEquals(OpportunityQueueable.RESENT_STATUS, oppAfter.StageName, 'Should be \'Reenviar\'');
        }
    }
}
