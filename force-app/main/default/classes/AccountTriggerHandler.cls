public with sharing class AccountTriggerHandler extends TriggerHandler {
    public static final String OPP_STAGE_CLOSED_WON = 'Closed Won';
    public static final String OPP_STAGE_CLOSED_LOST = 'Closed Lost';
    public static final String ACC_IS_ACTIVE = 'Si';
    public static final String ACC_IS_NOT_ACTIVE = 'No';
    public static final String CLOSE_REASON = 'Cerrada por Cuenta Inactiva';
    public static final String INFLUENTIAL_ROL  = 'Influyente';

    public AccountTriggerHandler() {}

    public override void afterUpdate() {
        List<Account> noActiveAccounts = getIsNotActiveAccounts(); 
        
        if(!noActiveAccounts.isEmpty()) {
            updateOppFromNoActiveAcc(noActiveAccounts);
            insertExClients(noActiveAccounts);
        }
    }

    /*
        @Description inserts exclients base on Account contacts
    */

    private static void insertExClients(List<Account> noActiveAccounts) {
        List<Contact> contactsWithinfluentialAcc = [SELECT 
                                                        Id, 
                                                        AccountId, 
                                                        Rol__c
                                                    FROM 
                                                        Contact 
                                                    WHERE 
                                                        AccountId IN (SELECT Id FROM Account WHERE Id IN : noActiveAccounts)
                                                    AND 
                                                        Rol__c = : INFLUENTIAL_ROL
                                                    WITH SECURITY_ENFORCED
                                                    LIMIT 1000];
        

        if(contactsWithinfluentialAcc.isEmpty()) {
            return;
        }

        List<Ex_Clientes__c> exClients = new List<Ex_Clientes__c>();

        for(Contact cont : contactsWithinfluentialAcc) {
            exClients.add( new Ex_Clientes__c(
                            Contacto__c = cont.Id,
                            Generado_automaticamente__c = true
                        ));
        }

        SObjectAccessDecision decision = Security.stripInaccessible(
                                        AccessType.CREATABLE,
                                        exClients);
        
        if(!decision.getRecords().isEmpty()) {
            INSERT decision.getRecords();
        }
    }

    /*
        @Description updates opp with no active account
    */

    private static void updateOppFromNoActiveAcc(List<Account> noActiveAccounts) {
        List<Opportunity> opportunities = [SELECT 
                                                Id,
                                                StageName, 
                                                Description
                                            FROM 
                                                Opportunity 
                                            WHERE 
                                                AccountId IN : noActiveAccounts 
                                            AND 
                                                StageName NOT IN (:OPP_STAGE_CLOSED_WON, :OPP_STAGE_CLOSED_LOST)
                                            WITH SECURITY_ENFORCED
                                            LIMIT 10000];

        if(opportunities.isEmpty()) {
            return;
        }

        for(Opportunity opp : opportunities) {
            opp.StageName = OPP_STAGE_CLOSED_LOST;
            opp.Description = CLOSE_REASON;
        }

        SObjectAccessDecision decision = Security.stripInaccessible(
                                        AccessType.UPDATABLE,
                                        opportunities);

        if(!decision.getRecords().isEmpty()) {
            UPDATE decision.getRecords();
        }
    }

    /*
        @Description get only Accounts that changed from active to inactive
    */

    private static List<Account> getIsNotActiveAccounts() {
        Map<Id, Account> oldValuesAccountMap = (Map<Id, Account>) Trigger.OldMap;
        List<Account> notActiveAccounts = new List<Account>();

        for(Account acc : (List<Account>)Trigger.new) {
            if(acc.Activa__c == ACC_IS_NOT_ACTIVE && acc.Activa__c != oldValuesAccountMap.get(acc.Id)?.Activa__c) {
                notActiveAccounts.add(acc);
            }
        }

        return notActiveAccounts; 
    }
}
