@isTest
public with sharing class Test_AccountTrigger {
    @TestSetup
    static void testData() {

        System.runAs(createUser('System Administrator')) {
            Account acc = new Account(Name = 'Test account', Activa__c = AccountTriggerHandler.ACC_IS_ACTIVE);
            insert acc;
    
            Contact cont = new Contact(LastName = 'Test Contact', 
                                        AccountId = acc.id,
                                        Rol__c = AccountTriggerHandler.INFLUENTIAL_ROL);
            insert cont;
    
            Opportunity opp = new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp',
                                                StageName = 'Perception Analysis', 
                                                CloseDate = Date.Today());
    
            insert opp;
        }
    }

    public static User createUser(String profileName) {
        Profile profileId = [SELECT Id FROM Profile WHERE Name =: profileName LIMIT 1];
        
        User usr = new User(LastName = 'alex',
                        FirstName='alex',
                        Alias = 'jliv',
                        Email = 'alex.alex@asdf.com',
                        Username = 'alex.alex@asdf.com',
                        ProfileId = profileId.id,
                        TimeZoneSidKey = 'GMT',
                        LanguageLocaleKey = 'en_US',
                        EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US'
                        );
        insert usr;

        return usr;
    }

    @isTest static void UPDATE_ACCOUNT_POSITIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            Account acc = [SELECT Id FROM Account WHERE Name = 'Test account' LIMIT 1];

            acc.Activa__c = AccountTriggerHandler.ACC_IS_NOT_ACTIVE;

            System.Test.startTest();
            UPDATE acc;
            System.Test.stopTest();

            Opportunity opp = [SELECT Id, StageName, Description FROM Opportunity WHERE AccountId =: acc.Id LIMIT 1];
            Contact cont = [SELECT Id FROM Contact WHERE AccountId =: acc.Id LIMIT 1];

            System.assertEquals(AccountTriggerHandler.OPP_STAGE_CLOSED_LOST, opp.StageName, 'Should be \'Closed lost\'');
            System.assertEquals(AccountTriggerHandler.CLOSE_REASON , opp.Description, 'Should have description \'Cerrada por Cuenta Inactiva\'');
            System.assertEquals( 1, [SELECT COUNT() FROM Ex_Clientes__c WHERE Contacto__c =: cont.Id AND Generado_automaticamente__c = true], 'Should be 1');
        }
    }

    @isTest static void UPDATE_ACCOUNT_NEGATIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            Account acc = [SELECT Id FROM Account WHERE Name = 'Test account' LIMIT 1];

            acc.Name = 'Testxd';

            System.Test.startTest();
            UPDATE acc;
            System.Test.stopTest();

            Opportunity opp = [SELECT Id, StageName, Description FROM Opportunity WHERE AccountId =: acc.Id LIMIT 1];
            Contact cont = [SELECT Id FROM Contact WHERE AccountId =: acc.Id LIMIT 1];

            System.assertNotEquals(AccountTriggerHandler.OPP_STAGE_CLOSED_LOST, opp.StageName, 'Shouldn\'t be \'Closed lost\'');
            System.assertNotEquals(AccountTriggerHandler.CLOSE_REASON , opp.Description, 'Shouldn\'t have description \'Cerrada por Cuenta Inactiva\'');
            System.assert([SELECT COUNT() FROM Ex_Clientes__c WHERE Contacto__c =: cont.Id AND Generado_automaticamente__c = true] == 0, 'Shouldn\'t greater than 0');
        }
    }
}
