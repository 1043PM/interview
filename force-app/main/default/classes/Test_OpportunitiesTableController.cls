@isTest
public with sharing class Test_OpportunitiesTableController {
    @TestSetup
    static void testData() {

        System.runAs(Test_AccountTrigger.createUser('System Administrator')) {
            Account acc = new Account(Name = 'Test account', Activa__c = AccountTriggerHandler.ACC_IS_ACTIVE);
            insert acc;

            List<Opportunity> opportunities = new List<Opportunity>();

            opportunities.add(new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp',
                                                StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_WON, 
                                                CloseDate = Date.Today()));

            opportunities.add(new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp2',
                                                StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_LOST, 
                                                CloseDate = Date.Today()));

            opportunities.add(new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp3',
                                                StageName = 'Perception Analysis', 
                                                CloseDate = Date.Today()));

            opportunities.add(new Opportunity(AccountId = acc.Id, 
                                                Name = 'Test opp4',
                                                StageName = AccountTriggerHandler.OPP_STAGE_CLOSED_WON, 
                                                CloseDate = Date.Today().addMonths(1)));                                    
            insert opportunities;
        }
    }

    @isTest static void getClosedOpportunitiesByAccountId_POSITIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            String accountId = acc.Id;

            System.Test.startTest();
            OpportunitiesTableController.Payload payload = OpportunitiesTableController.getClosedOpportunitiesByAccountId(accountId);
            System.Test.stopTest();

            System.assertEquals(2, payload.opportunities.size(), 'Should be only one opp');
        }
    }

    @isTest static void getClosedOpportunitiesByAccountId_NEGATIVE() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            String accountId = acc.Id;

            List<Opportunity> opportunities = new List<Opportunity>();

            for(Opportunity opp : [SELECT Id, CloseDate FROM Opportunity WHERE AccountId = : acc.Id]) {
                opp.CloseDate = Date.today().addMonths(1);

                opportunities.add(opp);
            }

            update opportunities;

            System.Test.startTest();
            OpportunitiesTableController.Payload payload = OpportunitiesTableController.getClosedOpportunitiesByAccountId(accountId);
            System.Test.stopTest();

            System.assertEquals(0, payload.opportunities.size(), 'Should be 0 opp');
        }
    }

    @isTest static void getClosedOpportunitiesByAccountId_NULL() {
        User us = [SELECT Id FROM User WHERE profile.Name = 'System Administrator' LIMIT 1];

        System.runAs(us) {
            System.Test.startTest();
            OpportunitiesTableController.Payload payload = OpportunitiesTableController.getClosedOpportunitiesByAccountId('');
            System.Test.stopTest();

            System.assertEquals(null, payload, 'Should be null');
        }
    }
}
