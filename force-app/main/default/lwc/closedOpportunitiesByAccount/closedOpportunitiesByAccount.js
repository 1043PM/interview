import { LightningElement, api, track } from 'lwc';
import getClosedOpportunitiesByAccountId from '@salesforce/apex/OpportunitiesTableController.getClosedOpportunitiesByAccountId';

export default class closedOpportunitiesByAccount extends LightningElement {
    columns = [
        { label: 'Nombre', fieldName: 'recordLink', type: 'url', typeAttributes: { label: {fieldName: 'Name'}}},
        { label: 'Cuenta', fieldName: 'accountName', type: 'text' },
        { label: 'Monto', fieldName: 'Amount', type: 'currency' },
        { label: 'Fecha de cierre', fieldName: 'CloseDate', type: 'date' },
        { label: 'Etapa', fieldName: 'StageName', type: 'text' },
        { label: 'Tipo', fieldName: 'Type', type: 'text' }
    ];

    @api recordId;
    @track data = [];

    connectedCallback() {
        getClosedOpportunitiesByAccountId({accountId : this.recordId})
        .then(payload => {
            this.data = payload.opportunities?.map(opp => {
                opp.recordLink = '/' + opp.Id;
                opp.accountName = opp.Account.Name;

                return opp;
            });
        }).catch(err => console.error(err));
    }
}